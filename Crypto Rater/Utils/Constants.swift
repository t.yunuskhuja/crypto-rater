//
//  Constants.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import UIKit

struct Constants {
    struct AppSize {
        static let reloadButtonWidth: CGFloat = 140
        static let sidePaddings: CGFloat = 16
    }
    
    struct API {
        static let apiKey: String = "2854a5c3399c288c9183d204216c9c5d706e7d55bb64cd5a67eda10db684a574"
        
        static let imgBaseURL: String = "https://cryptocompare.com"
    }
}
