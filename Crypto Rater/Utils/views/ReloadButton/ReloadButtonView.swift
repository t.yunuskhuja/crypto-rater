//
//  ReloadButtonView.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import UIKit
import SnapKit
import MaterialComponents.MaterialActivityIndicator

enum ReloadButtonState {
    case hidden
    case reloadButton
    case spinning
}

class ReloadButtonView: UIControl {
    //MARK: Properties
    private let indicator: MDCActivityIndicator = MDCActivityIndicator()
    private let containerStackView: UIStackView = UIStackView()
    private let reloadImageView: UIImageView = UIImageView()
    private let subtitleLabel: UILabel = UILabel()
    
    var buttonState: ReloadButtonState = .hidden {
        didSet {
            switch buttonState {
            case .hidden:
                isHidden = true
                indicator.stopAnimating()
            case .reloadButton:
                isHidden = false
                isUserInteractionEnabled = true
                indicator.stopAnimating()
                containerStackView.isHidden = false
            case .spinning:
                isHidden = false
                isUserInteractionEnabled = false
                indicator.startAnimating()
                containerStackView.isHidden = true
            }
        }
    }
    
    //MARK: Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    func initialize() {
        configure()
        addSubviews()
        setConstraints()
    }
    
    //MARK: Configurations
    private func configure() {
        self.backgroundColor = .clear
        configIndicator()
        configContainerStack()
        configReloadImage()
        configSubTitle()
    }
    
    private func configIndicator() {
        indicator.cycleColors = [UIColor.contentPrimary]
        indicator.tintColor = .white
        indicator.radius = 20
    }
    
    private func configContainerStack() {
        containerStackView.axis = .vertical
        containerStackView.alignment = .center
        containerStackView.spacing = 5
    }
    
    private func configReloadImage() {
        reloadImageView.image = #imageLiteral(resourceName: "ic_retry").withRenderingMode(.alwaysTemplate)
        reloadImageView.tintColor = UIColor.contentPrimary
    }
    
    private func configSubTitle() {
        subtitleLabel.text = "Retry"
        subtitleLabel.config(font: UIFont.systemFont(ofSize: 16, weight: .bold),
                             color: UIColor.contentPrimary,
                             numberOfLines: 0,
                             textAllignment: .center)
    }
    
    //MARK: Adding Subviews
    private func addSubviews() {
        self.add(subviews: indicator,
                 containerStackView)
        
        containerStackView.addArrangedSubview(reloadImageView)
        containerStackView.addArrangedSubview(subtitleLabel)
    }
    
    //MARK: Setting Constraints
    private func setConstraints() {
        indicator.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        containerStackView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        reloadImageView.snp.makeConstraints { (make) in
            make.width.height.equalTo(28)
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.sendActions(for: .touchUpInside)
    }
}
