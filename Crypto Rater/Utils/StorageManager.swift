//
//  StorageManager.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import Foundation

final public class StorageManager {
    private enum Key: String {
        case favoriteCoins = "_#_key_favCoins_#_"
        
        var value: String {
            return self.rawValue
        }
    }
    
    private let userDefaults: UserDefaults = UserDefaults.standard
    
    private init() {
        
    }
    
    public static var shared: StorageManager {
        struct Singleton {
            static let singleton = StorageManager()
        }
        return Singleton.singleton
    }
    
    public var listOfFavoriteCoins: [String] {
        return self.userDefaults.array(forKey: Key.favoriteCoins.value) as? [String] ?? []
    }
    
    public func setListOfFavoriteCoins(coins: [String]) {
        self.userDefaults.set(coins, forKey: Key.favoriteCoins.value)
    }
    
    public func removeFavoriteCoin(withSymbol symbol: String) {
        var favCoins: [String] = []
        if !listOfFavoriteCoins.isEmpty {
            favCoins = listOfFavoriteCoins
            favCoins.removeAll(where: { $0 == symbol })
            self.setListOfFavoriteCoins(coins: favCoins)
        }
    }
    
    public func appendFavoriteCoin(withSymbil symbol: String) {
        var coins: [String] = self.listOfFavoriteCoins
        coins.append(symbol)
        self.setListOfFavoriteCoins(coins: coins)
    }
}
