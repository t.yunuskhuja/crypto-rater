//
//  AppDelegate.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import UIKit

private(set)var app: App!

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.initApp(application: application)
        return true
    }
    
    //MARK: Private Methods
    private func initApp(application: UIApplication) {
        self.window = UIWindow(frame: UIScreen.main.bounds)
    
        app = App(application: application,
                  window: self.window!)
        app.launch()
    }
}

