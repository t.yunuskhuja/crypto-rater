//
//  UIViewController+Extensions.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import UIKit
import SnapKit

extension UIViewController {
    public var snpTopSafeConstraint: ConstraintItem {
        return self.view.safeAreaLayoutGuide.snp.top
    }
    
    public var snpBottomSafeConstraint: ConstraintItem {
        return self.view.safeAreaLayoutGuide.snp.bottom
    }
}

extension UIViewController {
    func hideKeyBoardOnTap() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyBoard))
        tapGesture.cancelsTouchesInView = false
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func hideKeyBoard() {
        self.view.endEditing(true)
    }
}
