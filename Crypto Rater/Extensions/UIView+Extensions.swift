//
//  UIView+Extensions.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import UIKit

extension UIView {
    public func add(subviews: UIView...) {
        for sv in subviews {
            self.addSubview(sv)
        }
    }
    
    public func addTapGesture(tapNumber: Int, target: AnyObject, action: Selector) {
        let tap = UITapGestureRecognizer(target: target, action: action)
        tap.numberOfTapsRequired = tapNumber
        addGestureRecognizer(tap)
        isUserInteractionEnabled = true
    }
}
