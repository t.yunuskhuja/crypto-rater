//
//  UILabel+Extensions.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import UIKit

extension UILabel {
    func config(font: UIFont?, color: UIColor, numberOfLines: Int, textAllignment: NSTextAlignment) {
        if let f = font {
            self.font = f
        }
        self.textColor = color
        self.numberOfLines = numberOfLines
        self.textAlignment = textAllignment
    }
}
