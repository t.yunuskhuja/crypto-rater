//
//  String+Extensions.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import Foundation

extension String {
    public func getAmountAsUSD() -> String {
        return "$\(self)"
    }
    
    public func getAmountAsEUR() -> String {
        return "€\(self)"
    }
    
    public func containsIgnoringCase(with other: String) -> Bool {
        return self.lowercased().contains(other.lowercased())
    }
}
