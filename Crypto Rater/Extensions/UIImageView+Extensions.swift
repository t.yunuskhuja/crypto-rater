//
//  UIImageView+Extensions.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import UIKit
import Kingfisher

extension UIImageView {
    func download(url: String,
                  placeholder: UIImage? = nil) {
        self.image = placeholder
        guard let url = URL(string: url) else {
            return
        }
        
        let resource = ImageResource(downloadURL: url)
        self.kf.indicatorType = .activity
        self.kf.setImage(with: resource, placeholder: placeholder, progressBlock: nil)
    }
}
