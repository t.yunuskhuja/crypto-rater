//
//  UIColor+Extensions.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import UIKit

extension UIColor {
    public convenience init(hex: UInt) {
        self.init(
            red: CGFloat((hex & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((hex & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(hex & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    public convenience init?(hex: String) {
        guard let h = UInt(hex) else {
            return nil
        }
        self.init(hex: h)
    }
}

extension UIColor {
    public static let backgroundPrimary: UIColor = UIColor(hex: 0xF2F4F8)
    
    public static let backgroundSecondary: UIColor = UIColor(hex: 0xFFFFFF)
    
    public static let contentPrimary: UIColor = UIColor(hex: 0x1E1E27)
    
    public static let contentSecondary: UIColor = UIColor(hex: 0xB3B4C6)
    
    public static let appGreen: UIColor = UIColor(hex: 0x6DCC4C)
}
