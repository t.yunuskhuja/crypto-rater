//
//  BaseViewController.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import UIKit

class BaseViewController: UIViewController {
    //MARK: Initialization
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyBoardOnTap()
    }
    
    //MARK: Actions
    public func showError(withMessage message: String) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: {_ in
            
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    public func showSuccess(withMessage message: String) {
        let alertController = UIAlertController(title: "Success", message: message, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style:.cancel, handler: {_ in
            
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    public func setRightNavBarItem(_ customView: UIView) {
        customView.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: customView)
    }
}
