//
//  AddCoinVC.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import UIKit
import SnapKit

protocol AddCoinVCProtocol: AnyObject {
    func initialize()
    func setReloadButton(_ state: ReloadButtonState)
    func reloadContent()
    func showError(_ message: String)
    func showGeneralSuccess()
    func performCVBatchUpdate(removedIndexPathes: [IndexPath],
                              insertedIndexPaths: [IndexPath])
}

class AddCoinVC: BaseViewController, AddCoinVCProtocol {
    //MARK: Properties
    private let searchBar: UISearchBar = UISearchBar()
    private let reloadButton = ReloadButtonView()
    private let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 16
        layout.minimumInteritemSpacing = 16
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        cv.showsVerticalScrollIndicator = false
        cv.showsHorizontalScrollIndicator = false
        cv.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 16, right: 0)
        return cv
    }()
    private let selectedCVCell = String(describing: AddCoinCVCell.self)
    private let notSelectedCVCell = String(describing: AddCoinCVCell.self)
    
    var presenter: AddCoinPresenter!
    
    //MARK: Initialization
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Available coins"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialize()
        presenter.viewDidLoad()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.presenter.viewWillDisappear()
    }
    
    func initialize() {
        configureViews()
        addSubviews()
        setConstraints()
    }
    
    //MARK: UI Configurations
    private func configureViews() {
        self.view.backgroundColor = UIColor.backgroundPrimary
        configSearchBar()
        configReloadButton()
        configCollectionView()
    }
    
    private func configSearchBar() {
        searchBar.barTintColor = UIColor.backgroundPrimary
        searchBar.backgroundColor = UIColor.backgroundPrimary
        searchBar.isTranslucent = true
        searchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        searchBar.placeholder = "Search"
        searchBar.alpha = 0
        searchBar.delegate = self
    }
    
    private func configReloadButton() {
        reloadButton.buttonState = .hidden
        reloadButton.addTarget(self, action: #selector(onReloadButtonTapped), for: .touchUpInside)
    }
    
    private func configCollectionView() {
        collectionView.alpha = 0
        collectionView.register(AddCoinCVCell.self, forCellWithReuseIdentifier: selectedCVCell)
        collectionView.register(AddCoinCVCell.self, forCellWithReuseIdentifier: notSelectedCVCell)
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    //MARK: Adding Subviews
    private func addSubviews() {
        self.view.add(subviews: searchBar,
                      reloadButton,
                      collectionView)
    }
    
    //MARK: Setting Constraints
    private func setConstraints() {
        searchBar.snp.makeConstraints { make in
            make.top.equalTo(snpTopSafeConstraint)
            make.leading.trailing.equalToSuperview()
        }
        
        reloadButton.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.equalTo(Constants.AppSize.reloadButtonWidth)
        }
        
        collectionView.snp.makeConstraints { make in
            make.top.equalTo(searchBar.snp.bottom)
            make.leading.trailing.equalToSuperview().inset(Constants.AppSize.sidePaddings)
            make.bottom.equalToSuperview()
        }
    }
    
    //MARK: Actions
    func setReloadButton(_ state: ReloadButtonState) {
        self.reloadButton.buttonState = state
    }
    
    func reloadContent() {
        self.collectionView.reloadData()
        self.collectionView.alpha = 1
        self.searchBar.alpha = 1
    }
    
    func showError(_ message: String) {
        self.showError(withMessage: message)
    }
    
    func showGeneralSuccess() {
        self.showSuccess(withMessage: "Operation successful!")
    }
    
    func performCVBatchUpdate(removedIndexPathes: [IndexPath], insertedIndexPaths: [IndexPath]) {
        self.collectionView.performBatchUpdates({
            self.collectionView.deleteItems(at: removedIndexPathes)
            self.collectionView.insertItems(at: insertedIndexPaths)
        })
    }
    
    //MARK: Private Actions
    @objc private func onReloadButtonTapped() {
        presenter.onReloadButtonTapped()
    }
}
//MARK: Delegate Methods
extension AddCoinVC: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.presenter.didSearch(withText: searchBar.text ?? "")
    }
}

extension AddCoinVC: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let widthAndHeight = (collectionView.bounds.width - 16) / 2
        return CGSize(width: widthAndHeight, height: widthAndHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.presenter.dataManager.filteredCoinsCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let (item, isSelected) = self.presenter.dataManager.getFilteredCoin(atIndex: indexPath.item) else {
            return UICollectionViewCell()
        }
        
        if isSelected {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: selectedCVCell, for: indexPath) as! AddCoinCVCell
            cell.populateWith(model: item,
                              isSelected: true)
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: notSelectedCVCell, for: indexPath) as! AddCoinCVCell
            cell.populateWith(model: item,
                              isSelected: false)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.presenter.didSelectCellAtIndex(indexPath.item)
    }
}
