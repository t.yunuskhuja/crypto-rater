//
//  AddCoinCVCell.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import UIKit
import SnapKit

class AddCoinCVCell: UICollectionViewCell {
    //MARK: Properties
    private let containerView = UIView()
    private let iconImageView = UIImageView()
    private let selectedStateImageView = UIImageView()
    private let nameLabel = UILabel()
    
    //MARK: Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        configure()
        addSubviews()
        setConstraints()
    }
    
    //MARK: Configurations
    private func configure() {
        self.backgroundColor = .clear
        
        containerView.backgroundColor = UIColor.backgroundSecondary
        containerView.layer.cornerRadius = 16
        
        configImages()
        configName()
    }
    
    private func configImages() {
        iconImageView.contentMode = .scaleAspectFit
        
        selectedStateImageView.contentMode = .scaleAspectFit
        selectedStateImageView.image = #imageLiteral(resourceName: "ic_check").withRenderingMode(.alwaysTemplate)
        selectedStateImageView.tintColor = UIColor.appGreen
        selectedStateImageView.alpha = 0
    }
    
    private func configName() {
        nameLabel.config(font: UIFont.systemFont(ofSize: 14,
                                                 weight: .bold),
                         color: UIColor.contentPrimary,
                         numberOfLines: 2,
                         textAllignment: .center)
        nameLabel.lineBreakMode = .byTruncatingMiddle
    }
    
    //MARK: Adding Subviews
    private func addSubviews() {
        containerView.add(subviews: iconImageView,
                          selectedStateImageView,
                          nameLabel)
        
        contentView.addSubview(containerView)
    }
    
    //MARK: Setting Constraints
    private func setConstraints() {
        containerView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        iconImageView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.size.equalTo(48)
            make.centerY.equalToSuperview().offset(-16)
        }
        
        selectedStateImageView.snp.makeConstraints { make in
            make.edges.equalTo(iconImageView)
        }
        
        nameLabel.snp.makeConstraints { make in
            make.top.equalTo(iconImageView.snp.bottom).offset(8)
            make.leading.trailing.equalToSuperview().inset(4)
        }
    }
    
    //MARK: Actions
    public func populateWith(model: CoinVM, isSelected: Bool) {
        self.nameLabel.text = model.name
        self.iconImageView.download(url: model.iconUrl,
                                    placeholder: nil)
        
        if isSelected {
            self.iconImageView.alpha = 0
            self.selectedStateImageView.alpha = 1
            self.nameLabel.textColor = UIColor.contentSecondary
        } else {
            self.selectedStateImageView.alpha = 0
            self.iconImageView.alpha = 1
            self.nameLabel.textColor = UIColor.contentPrimary
        }
    }
}
