//
//  AddCoinPresenter.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import Foundation

protocol AddCoinPresenterProtocol: AnyObject {
    var dataManager: AddCoinDataManager { get }
    
    func viewDidLoad()
    func viewWillDisappear()
    func onReloadButtonTapped()
    func didSelectCellAtIndex(_ i: Int)
    func didSearch(withText text: String)
}

protocol AddCoinInteractorToPresenterProtocol: AnyObject {
    func didLoadAllCoins(_ coins: [CoinVM],
                         error: Error?)
}

protocol CoinAddedDelegate: AnyObject {
    func userDidAddCoins(_ didAdd: Bool)
}

class AddCoinPresenter: AddCoinPresenterProtocol {
    weak var view: AddCoinVCProtocol!
    var interactor: AddCoinInteractor!
    var router: AddCoinRouterProtocol!
    
    weak var coinAddedDelegate: CoinAddedDelegate?
    
    private(set) var dataManager: AddCoinDataManager = AddCoinDataManager()
    
    func viewDidLoad() {
        self.loadAllCoins()
    }
    
    func viewWillDisappear() {
        self.coinAddedDelegate?.userDidAddCoins(!self.dataManager.isSelectedCoinsEmpty)
    }
    
    func onReloadButtonTapped() {
        self.loadAllCoins()
    }
    
    func didSelectCellAtIndex(_ i: Int) {
        guard let (item, isSelected) = self.dataManager.getFilteredCoin(atIndex: i) else {
            return
        }
        
        if isSelected {
            self.dataManager.removeUnSelectedCoin(item)
        } else {
            self.dataManager.addSelectedCoin(item)
        }
        
        self.view.reloadContent()
    }
    
    func didSearch(withText text: String) {
        let previouslySearchedItems = dataManager.filteredCoins
        self.dataManager.search(text, completion: { [weak self] in
            guard let self = self else {
                return
            }
            
            let currentSearchedItems = self.dataManager.filteredCoins
            let differenceWithPreviousSearch = currentSearchedItems.difference(from: previouslySearchedItems)
            
            var removedIndexPathes: [IndexPath] = []
            var insertedIndexPathes: [IndexPath] = []
            
            for difference in differenceWithPreviousSearch {
                switch difference {
                case let .remove(offset, element: _, associatedWith: _):
                    removedIndexPathes.append(IndexPath(item: offset, section: 0))
                case let.insert(offset, element: _, associatedWith: _):
                    insertedIndexPathes.append(IndexPath(item: offset, section: 0))
                }
            }
            
            self.view.performCVBatchUpdate(removedIndexPathes: removedIndexPathes,
                                           insertedIndexPaths: insertedIndexPathes)
        })
    }
    
    private func loadAllCoins() {
        self.view.setReloadButton(.spinning)
        
        self.interactor.loadCoinList()
    }
}

//MARK: Interactor->Presenter
extension AddCoinPresenter: AddCoinInteractorToPresenterProtocol {
    func didLoadAllCoins(_ coins: [CoinVM], error: Error?) {
        if let e = error {
            self.view.setReloadButton(.reloadButton)
            self.view.showError(e.localizedDescription)
        } else {
            self.dataManager.set(allCoins: coins)
            self.view.setReloadButton(.hidden)
            self.view.reloadContent()
        }
    }
}
