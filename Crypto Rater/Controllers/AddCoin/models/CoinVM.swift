//
//  CoinVM.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import Foundation

class CoinVM: Equatable {
    let id: String
    let code: String
    let name: String
    let iconUrl: String
    
    init(coinInfoModel: CoinInfoModel) {
        self.id = coinInfoModel.id ?? "0"
        self.code = coinInfoModel.symbol ?? ""
        self.name = coinInfoModel.fullName ?? "Coin"
        self.iconUrl = Constants.API.imgBaseURL + (coinInfoModel.imageURL ?? "")
    }
    
    public static func == (lhs: CoinVM, rhs: CoinVM) -> Bool {
        return lhs.id == rhs.id
    }
}
