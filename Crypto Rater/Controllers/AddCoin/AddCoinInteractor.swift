//
//  AddCoinInteractor.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import Foundation

protocol AddCoinInteractorProtocol: AnyObject {
    func loadCoinList()
}

class AddCoinInteractor: AddCoinInteractorProtocol {
    weak var presenter: AddCoinInteractorToPresenterProtocol!
    private let networkManager = NetworkManager()
    
    func loadCoinList() {
        //Need to exclude coins which are already favorite
        
        let favoriteCoins: [String] = StorageManager.shared.listOfFavoriteCoins
        
        self.networkManager.getCoinList(completion: { (result) in
            guard let presenter = self.presenter else {
                return
            }
            
            switch result {
            case .success(let coinsModel):
                var allCoins: [CoinVM] = coinsModel.data.map({CoinVM(coinInfoModel: $0.value)})
                if !favoriteCoins.isEmpty {
                    allCoins = allCoins.filter({!favoriteCoins.contains($0.code)})
                }
                
                allCoins = allCoins.sorted(by: {$0.id < $1.id})
                DispatchQueue.main.async {
                    presenter.didLoadAllCoins(allCoins,
                                              error: nil)
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    presenter.didLoadAllCoins([],
                                              error: error)
                }
            }
        })
    }
}
