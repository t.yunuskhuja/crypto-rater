//
//  AddCoinDataManager.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import Foundation

class AddCoinDataManager {
    private let debouncer = Debouncer(delay: 0.3)
    
    private var allCoins: [CoinVM] = []
    
    public private(set) var filteredCoins: [CoinVM] = []
    
    private var selectedCoins: [CoinVM] = []
    
    public var isSelectedCoinsEmpty: Bool {
        return self.selectedCoins.isEmpty
    }
    
    public func set(allCoins: [CoinVM]) {
        self.allCoins = allCoins
        self.filteredCoins = allCoins
    }
    
    public var filteredCoinsCount: Int {
        return self.filteredCoins.count
    }
    
    public func getFilteredCoin(atIndex i: Int) -> (CoinVM, Bool)? {
        if self.filteredCoins.indices.contains(i) {
            return (self.filteredCoins[i], self.selectedCoins.contains(where: {$0.id == self.filteredCoins[i].id}))
        } else {
            return nil
        }
    }
    
    public func addSelectedCoin(_ coin: CoinVM) {
        if !self.selectedCoins.contains(where: { $0.id == coin.id}) {
            self.selectedCoins.append(coin)
            StorageManager.shared.appendFavoriteCoin(withSymbil: coin.code)
        }
    }
    
    public func removeUnSelectedCoin(_ coin: CoinVM) {
        self.selectedCoins.removeAll(where: {$0.id == coin.id})
        StorageManager.shared.removeFavoriteCoin(withSymbol: coin.code)
    }
    
    //MARK: Search Logic
    public func search(_ text: String, completion: @escaping () -> Void) {
        debouncer.run { [weak self] in
            if text.isEmpty {
                self?.setFilter(predicate: nil)
                completion()
            } else {
                self?.searchText(text)
                completion()
            }
        }
    }
    
    private func searchText(_ searchText: String) {
        let latinSearchText = searchText.applyingTransform(.latinToCyrillic, reverse: true)
        let cryllicSearchText = searchText.applyingTransform(.latinToCyrillic, reverse: false)
        
        setFilter { (item) -> Bool in
            let name = "\(item.name)"
    
            var containsSearchText = name.containsIgnoringCase(with: searchText)
            
            if !containsSearchText, let latinText = latinSearchText {
                containsSearchText = name.containsIgnoringCase(with: latinText)
            }
            
            if !containsSearchText, let cyrillicText = cryllicSearchText {
                containsSearchText = name.containsIgnoringCase(with: cyrillicText)
            }
            
            return containsSearchText
        }
    }
    
    private func setFilter(predicate: ((CoinVM) -> Bool)?) {
        guard let p = predicate else {
            self.filteredCoins = allCoins
            return
        }
        
        self.filteredCoins = self.allCoins.filter(p)
    }
}
