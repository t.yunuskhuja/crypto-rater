//
//  AddCoinRouter.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import UIKit

protocol AddCoinRouterProtocol: AnyObject {
    static func createModule(coinDidAddDelegate: CoinAddedDelegate) -> UIViewController
}

class AddCoinRouter: AddCoinRouterProtocol {
    static func createModule(coinDidAddDelegate: CoinAddedDelegate) -> UIViewController {
        let vc = AddCoinVC()
        let presenter = AddCoinPresenter()
        let interactor = AddCoinInteractor()
        let router = AddCoinRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        presenter.coinAddedDelegate = coinDidAddDelegate
        
        return vc
    }
}
