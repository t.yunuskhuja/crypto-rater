//
//  FavoriteCoinRateVM.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import Foundation

class FavoriteCoinRateVM {
    let name: String
    let usdRate: String
    let eurRate: String
    
    init(name: String,
         rate: ExchangeRateValue) {
        self.name = name
        self.usdRate = "\(rate.usd)".getAmountAsUSD()
        self.eurRate = "\(rate.eur)".getAmountAsEUR()
    }
}
