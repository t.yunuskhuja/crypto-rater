//
//  FavoriteCoinsVC.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import UIKit
import SnapKit

protocol FavoriteCoinsVCProtocol: AnyObject {
    func initialize()
    func setReloadButton(_ state: ReloadButtonState)
    func reloadContent()
    func reloadContentAsEmpty()
    func hideContent()
    func showError(_ message: String)
    func toggleTableViewInteraction(_ enabled: Bool)
    func hideRefresh()
}

class FavoriteCoinsVC: BaseViewController, FavoriteCoinsVCProtocol {
    //MARK: Properties
    private let reloadButton = ReloadButtonView()
    private let tableView = UITableView()
    private let tvCell = String(describing: FavoriteCoinsTVCell.self)
    private let emptyLabel = UILabel()
    private let refreshControl = UIRefreshControl()
    private let addCoinImageView = UIImageView()
    
    var presenter: FavoriteCoinsPresenter!
    
    //MARK: Initialization
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Favorite Coins"
        self.setAddCoinImageView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialize()
        presenter.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.presenter.viewDidAppear()
    }
    
    func initialize() {
        self.configureViews()
        self.addSubviews()
        self.setConstraints()
    }
    
    //MARK: UI Configurations
    private func configureViews() {
        self.view.backgroundColor = UIColor.backgroundPrimary
        self.configReloadButton()
        self.configTableView()
        self.configEmptyState()
    }
    
    private func configReloadButton() {
        reloadButton.buttonState = .hidden
        reloadButton.addTarget(self, action: #selector(onReloadButtonTapped), for: .touchUpInside)
    }
    
    private func configTableView() {
        tableView.backgroundColor = .clear
        tableView.tableFooterView = UIView()
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .singleLine
        tableView.contentInset.bottom = 20
        tableView.rowHeight = 64
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.alpha = 0
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(FavoriteCoinsTVCell.self, forCellReuseIdentifier: self.tvCell)
        
        refreshControl.addTarget(self, action: #selector(self.refreshContent), for: .valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    private func configEmptyState() {
        emptyLabel.text = "No favorite coins"
        emptyLabel.config(font: UIFont.systemFont(ofSize: 18,
                                                  weight: .bold),
                          color: UIColor.contentSecondary,
                          numberOfLines: 0,
                          textAllignment: .center)
        emptyLabel.alpha = 0
    }
    
    //MARK: Adding Subviews
    private func addSubviews() {
        self.view.add(subviews: reloadButton,
                      tableView,
                      emptyLabel)
    }
    
    //MARK: Setting Constraints
    private func setConstraints() {
        reloadButton.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.equalTo(Constants.AppSize.reloadButtonWidth)
        }
        
        tableView.snp.makeConstraints { make in
            make.top.equalTo(snpTopSafeConstraint)
            make.leading.trailing.bottom.equalToSuperview()
        }
        
        emptyLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(18)
        }
    }
    
    //MARK: Public Actions
    func setReloadButton(_ state: ReloadButtonState) {
        self.reloadButton.buttonState = state
    }
    
    func reloadContent() {
        self.tableView.reloadData()
        self.tableView.alpha = 1
        self.emptyLabel.alpha = 0
    }
    
    func reloadContentAsEmpty() {
        self.tableView.alpha = 0
        self.emptyLabel.alpha = 1
    }
    
    func hideContent() {
        self.tableView.alpha = 0
        self.emptyLabel.alpha = 0
    }
    
    func showError(_ message: String) {
        self.showError(withMessage: message)
    }
    
    func toggleTableViewInteraction(_ enabled: Bool) {
        self.tableView.isUserInteractionEnabled = enabled
    }
    
    func hideRefresh() {
        self.refreshControl.endRefreshing()
    }
    
    //MARK: Private Actions
    @objc private func onReloadButtonTapped() {
        self.presenter.onReloadButtonTapped()
    }
    
    @objc private func refreshContent() {
        self.presenter.onRefreshCalled()
    }
    
    @objc private func onAddCoinTapped() {
        self.presenter.onAddCoinTapped()
    }
    
    private func setAddCoinImageView() {
        addCoinImageView.contentMode = .scaleAspectFit
        addCoinImageView.image = #imageLiteral(resourceName: "ic_add").withRenderingMode(.alwaysTemplate)
        addCoinImageView.tintColor = UIColor.contentPrimary
        
        addCoinImageView.addTapGesture(tapNumber: 1, target: self, action: #selector(onAddCoinTapped))
        
        self.setRightNavBarItem(addCoinImageView)
    }
}

//MARK: Delegate Methods
extension FavoriteCoinsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.dataManager.exchangeRatesCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let item = self.presenter.dataManager.getExchangeRate(atIndex: indexPath.row) else {
            return UITableViewCell()
        }
        
        var cell = tableView.dequeueReusableCell(withIdentifier: tvCell) as? FavoriteCoinsTVCell
        if cell == nil {
            cell = FavoriteCoinsTVCell(style: .default, reuseIdentifier: tvCell)
        }
        
        cell?.populateWithModel(item)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if presenter.tableViewDidDelete(atIndex: indexPath.row) {
                tableView.deleteRows(at: [indexPath], with: .fade)
                presenter.cellDeletionDidFinish()
            }
        }
    }
}
