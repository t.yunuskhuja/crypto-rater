//
//  FavoriteCoinsDataManager.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import Foundation

class FavoriteCoinsDataManager {
    private var exchangeRatesVM: [FavoriteCoinRateVM] = []
    
    public func set(exchangeRatesVM: [FavoriteCoinRateVM]) {
        self.exchangeRatesVM = exchangeRatesVM
    }
    
    public func removeElement(atIndex i: Int) -> FavoriteCoinRateVM? {
        if self.exchangeRatesVM.indices.contains(i) {
            return self.exchangeRatesVM.remove(at: i)
        } else {
            return nil
        }
    }
    
    public var isExchangeRatesEmpty: Bool {
        return self.exchangeRatesVM.isEmpty
    }
    
    public var exchangeRatesCount: Int {
        return self.exchangeRatesVM.count
    }
    
    public func getExchangeRate(atIndex i: Int) -> FavoriteCoinRateVM? {
        if self.exchangeRatesVM.indices.contains(i) {
            return self.exchangeRatesVM[i]
        } else {
            return nil
        }
    }
}
