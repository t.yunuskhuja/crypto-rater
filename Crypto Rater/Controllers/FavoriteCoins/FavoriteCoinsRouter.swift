//
//  FavoriteCoinsRouter.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import UIKit

protocol FavoriteCoinsRouterProtocol: AnyObject {
    static func createModule() -> UIViewController
    
    func showAddCoinController(fromController sourceVC: Any?,
                               delegate: CoinAddedDelegate)
}

class FavoriteCoinsRouter: FavoriteCoinsRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = FavoriteCoinsVC()
        let presenter = FavoriteCoinsPresenter()
        let interactor = FavoriteCoinsInteractor()
        let router = FavoriteCoinsRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    func showAddCoinController(fromController sourceVC: Any?,
                               delegate: CoinAddedDelegate) {
        guard let sourceVC = sourceVC as? UIViewController else {
            return
        }
        
        let destinationController = AddCoinRouter.createModule(coinDidAddDelegate: delegate)
        
        sourceVC.navigationController?.pushViewController(destinationController,
                                                          animated: true)
    }
}
