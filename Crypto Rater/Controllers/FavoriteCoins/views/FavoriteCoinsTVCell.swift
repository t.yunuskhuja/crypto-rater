//
//  FavoriteCoinsTVCell.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import UIKit
import SnapKit

class FavoriteCoinsTVCell: UITableViewCell {
    //MARK: Properties
    private let containerView: UIView = UIView()
    private let coinNameLabel: UILabel = UILabel()
    private let usdPriceLabel: UILabel = UILabel()
    private let eurPriceLabel: UILabel = UILabel()
    
    //MARK: Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        configure()
        addSubviews()
        setConstraints()
    }
    
    //MARK: Configurations
    private func configure() {
        self.backgroundColor = .clear
        self.selectionStyle = .none
        self.containerView.backgroundColor = .clear
        self.configLabels()
    }
    
    private func configLabels() {
        coinNameLabel.config(font: UIFont.systemFont(ofSize: 18,
                                                     weight: .bold),
                             color: UIColor.contentPrimary,
                             numberOfLines: 1,
                             textAllignment: .left)
        
        usdPriceLabel.config(font: UIFont.systemFont(ofSize: 16,
                                                     weight: .semibold),
                             color: UIColor.contentPrimary,
                             numberOfLines: 1,
                             textAllignment: .right)
        
        eurPriceLabel.config(font: UIFont.systemFont(ofSize: 16,
                                                     weight: .semibold),
                             color: UIColor.contentPrimary,
                             numberOfLines: 1,
                             textAllignment: .right)
    }
    
    //MARK: Adding Subviews
    private func addSubviews() {
        self.containerView.add(subviews: coinNameLabel,
                               usdPriceLabel,
                               eurPriceLabel)
        
        contentView.addSubview(containerView)
    }
    
    //MARK: Setting Constraints
    private func setConstraints() {
        containerView.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(Constants.AppSize.sidePaddings)
        }
        
        coinNameLabel.snp.makeConstraints { make in
            make.leading.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.35)
        }
        
        usdPriceLabel.snp.makeConstraints { make in
            make.trailing.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.65)
            make.top.equalToSuperview().offset(8)
        }
        
        eurPriceLabel.snp.makeConstraints { make in
            make.trailing.width.equalTo(usdPriceLabel)
            make.bottom.equalToSuperview().offset(-8)
        }
    }
    
    //MARK: Actions
    public func populateWithModel(_ model: FavoriteCoinRateVM) {
        self.coinNameLabel.text = model.name
        self.usdPriceLabel.text = model.usdRate
        self.eurPriceLabel.text = model.eurRate
    }
}
