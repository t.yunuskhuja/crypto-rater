//
//  FavoriteCoinsInteractor.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import Foundation

protocol FavoriteCoinsInteractorProtocol: AnyObject {
    func loadExchangeRates(isRefresh: Bool)
}

class FavoriteCoinsInteractor: FavoriteCoinsInteractorProtocol {
    weak var presenter: FavoriteCoinsInteractorToPresenterProtocol!
    private let networkManager = NetworkManager()
    
    func loadExchangeRates(isRefresh: Bool) {
        let favoriteCoinCodes: String = StorageManager.shared.listOfFavoriteCoins.joined(separator: ",")
        
        self.networkManager.getExchangeRatesFor(coins: favoriteCoinCodes,
                                                currencies: "USD,EUR",
                                                completion: { (result) in
            guard let presenter = self.presenter else {
                return
            }
            
            switch result {
            case .success(let exchangeRateModel):
                let favoriteCoinRates: [FavoriteCoinRateVM] = exchangeRateModel.map({FavoriteCoinRateVM(name: $0.key, rate: $0.value)}).sorted(by: {$0.name < $1.name })
                
                DispatchQueue.main.async {
                    presenter.didLoadExchangeRates(favoriteCoinRates, error: nil, isRefresh: isRefresh)
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    presenter.didLoadExchangeRates([], error: error, isRefresh: isRefresh)
                }
            }
        })
    }
}
