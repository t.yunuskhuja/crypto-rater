//
//  FavoriteCoinsPresenter.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import Foundation

protocol FavoriteCoinsPresenterProtocol: AnyObject {
    var dataManager: FavoriteCoinsDataManager { get }
    
    func viewDidLoad()
    func viewDidAppear()
    func onReloadButtonTapped()
    func tableViewDidDelete(atIndex i: Int) -> Bool
    func cellDeletionDidFinish()
    func onRefreshCalled()
    func onAddCoinTapped()
}

protocol FavoriteCoinsInteractorToPresenterProtocol: AnyObject {
    func didLoadExchangeRates(_ rates: [FavoriteCoinRateVM],
                              error: Error?,
                              isRefresh: Bool)
}

class FavoriteCoinsPresenter: FavoriteCoinsPresenterProtocol {
    weak var view: FavoriteCoinsVCProtocol!
    var interactor: FavoriteCoinsInteractor!
    var router: FavoriteCoinsRouterProtocol!
    
    private(set) var dataManager: FavoriteCoinsDataManager = FavoriteCoinsDataManager()
    
    private var didAddCoin: Bool = false
    
    func viewDidLoad() {
        if StorageManager.shared.listOfFavoriteCoins.isEmpty {
            self.view.reloadContentAsEmpty()
        } else {
            self.loadExchangeRates()
        }
    }
    
    func viewDidAppear() {
        if self.didAddCoin {
            self.view.hideContent()
            self.loadExchangeRates()
        }
    }
    
    private func loadExchangeRates(isRefresh: Bool = false) {
        if !isRefresh {
            self.view.setReloadButton(.spinning)
        }
        
        self.interactor.loadExchangeRates(isRefresh: isRefresh)
    }
    
    func onReloadButtonTapped() {
        self.loadExchangeRates()
    }
    
    func tableViewDidDelete(atIndex i: Int) -> Bool {
        guard let item = self.dataManager.removeElement(atIndex: i) else {
            return false
        }
        
        let code = item.name
        StorageManager.shared.removeFavoriteCoin(withSymbol: code)
        
        return true
    }
    
    func cellDeletionDidFinish() {
        self.reloadData()
    }
    
    func onRefreshCalled() {
        self.view.toggleTableViewInteraction(false)
        
        self.loadExchangeRates(isRefresh: true)
    }
    
    func onAddCoinTapped() {
        self.router.showAddCoinController(fromController: self.view,
                                          delegate: self)
    }
    
    private func reloadData() {
        if self.dataManager.isExchangeRatesEmpty {
            self.view.reloadContentAsEmpty()
        } else {
            self.view.reloadContent()
        }
    }
    
    private func enableTVAfterRefresh() {
        self.view.toggleTableViewInteraction(true)
        self.view.hideRefresh()
    }
}

extension FavoriteCoinsPresenter: CoinAddedDelegate {
    func userDidAddCoins(_ didAdd: Bool) {
        self.didAddCoin = didAdd
    }
}

//MARK: Interactor->Presenter
extension FavoriteCoinsPresenter: FavoriteCoinsInteractorToPresenterProtocol {
    func didLoadExchangeRates(_ rates: [FavoriteCoinRateVM],
                              error: Error?,
                              isRefresh: Bool) {
        if let e = error {
            if isRefresh {
                self.enableTVAfterRefresh()
            } else {
                self.view.setReloadButton(.reloadButton)
            }
            self.view.showError(e.localizedDescription)
        } else {
            self.dataManager.set(exchangeRatesVM: rates)
            self.view.setReloadButton(.hidden)
            if isRefresh {
                self.enableTVAfterRefresh()
            }
            self.reloadData()
        }
    }
}
