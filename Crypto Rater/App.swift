//
//  App.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import UIKit

final public class App {
    public let application: UIApplication
    public let window: UIWindow
    
    public init(application: UIApplication,
                window: UIWindow) {
        self.application = application
        self.window = window
    }
    
    public func launch() {
        self.configNavBar()
        
        self.setMainRootController()
    }
    
    private func setMainRootController() {
        let mainController = newNavCon(rootController: FavoriteCoinsRouter.createModule())
        
        self.changeWindow(rootController: mainController)
    }
    
    private func newNavCon(rootController: UIViewController) -> UINavigationController {
        let navCon = UINavigationController(rootViewController: rootController)
        return navCon
    }
    
    private func changeWindow(rootController: UIViewController) {
        self.window.rootViewController = rootController
        self.window.makeKeyAndVisible()
    }
    
    
    //MARK: Universal UI Updates
    private func configNavBar() {
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().backgroundColor = UIColor.clear
        UINavigationBar.appearance().isTranslucent = true
        UINavigationBar.appearance().tintColor = UIColor.contentPrimary
        UINavigationBar.appearance().titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.contentPrimary,
            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 18)]
        
        let backImage = #imageLiteral(resourceName: "ic_back")
        UINavigationBar.appearance().backIndicatorImage = backImage
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = backImage
        let bbItem = UIBarButtonItem.appearance(whenContainedInInstancesOf: [UINavigationBar.self])
        bbItem.setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -1000, vertical: 0), for: .default)
    }
}
