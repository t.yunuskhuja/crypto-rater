//
//  RequestParam.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import Foundation

enum RequestParam: String {
    case fsyms, tsyms, apiKey = "api_key", summary
    
    var value: String {
        return self.rawValue
    }
}
