//
//  CoinInfoResponseModel.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import Foundation

struct CoinInfoResponseModel: Decodable {
    let data: [String: CoinInfoModel]
    
    enum CodingKeys: String, CodingKey {
        case data = "Data"
    }
}

struct CoinInfoModel: Decodable {
    let id, imageURL, symbol, fullName: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case imageURL = "ImageUrl"
        case symbol = "Symbol"
        case fullName = "FullName"
    }
}
