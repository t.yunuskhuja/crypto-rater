//
//  ExchangeRateResponseModel.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import Foundation

struct ExchangeRateValue: Decodable {
    let usd, eur: Double
    
    enum CodingKeys: String, CodingKey {
        case usd = "USD"
        case eur = "EUR"
    }
}

typealias ExchangeRateResponseModel = [String: ExchangeRateValue]
