//
//  NetworkManager.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import Moya

protocol Networkable {
    var provider: MoyaProvider<CryptoCompareAPI> { get }
    
    func getExchangeRatesFor(coins: String, currencies: String, completion: @escaping (Result<ExchangeRateResponseModel, Error>) -> ())
}

class NetworkManager: Networkable {
    var provider = MoyaProvider<CryptoCompareAPI>(plugins: [NetworkLoggerPlugin()])
    
    func getExchangeRatesFor(coins: String, currencies: String, completion: @escaping (Result<ExchangeRateResponseModel, Error>) -> ()) {
        request(target: .multiSymbolsPrice(coins: coins, currencies: currencies), completion: completion)
    }
    
    func getCoinList(completion: @escaping (Result<CoinInfoResponseModel, Error>) -> ()) {
        request(target: .coinlist, completion: completion)
    }
}

private extension NetworkManager {
    private func request<T: Decodable>(target: CryptoCompareAPI, completion: @escaping (Result<T, Error>) -> ()) {
        provider.request(target) { result in
            switch result {
            case let .success(response):
                do {
                    let results = try JSONDecoder().decode(T.self, from: response.data)
                    completion(.success(results))
                } catch let error {
                    completion(.failure(error))
                }
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }
}
