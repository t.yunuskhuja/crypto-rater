//
//  CryptoCompareAPI.swift
//  Crypto Rater
//
//  Created by yunus on 15/05/22.
//

import Moya

enum CryptoCompareAPI {
    case multiSymbolsPrice(coins: String, currencies: String)
    case coinlist
}

extension CryptoCompareAPI: TargetType {
    var baseURL: URL {
        guard let url = URL(string: "https://min-api.cryptocompare.com/data/") else { fatalError("could not set baseURL") }
        return url
    }
    
    var path: String {
        switch self {
        case .multiSymbolsPrice:
            return "pricemulti"
        case .coinlist:
            return "all/coinlist"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .multiSymbolsPrice,
                .coinlist:
            return .get
        }
    }
    
    var task: Task {
        switch self {
        case .multiSymbolsPrice(let coins, let currencies):
            return .requestParameters(parameters: [RequestParam.fsyms.value: coins,
                                                   RequestParam.tsyms.value: currencies],
                                      encoding: URLEncoding.queryString)
        case .coinlist:
            return .requestParameters(parameters: [RequestParam.apiKey.value: Constants.API.apiKey,
                                                   RequestParam.summary.value: true],
                                      encoding: URLEncoding.queryString)
        }
    }
    
    var validationType: ValidationType {
        return .successCodes
    }
    
    var headers: [String : String]? {
        return ["Content-type": "application/json"]
    }
}
